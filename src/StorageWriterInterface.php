<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-storage-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Storage;

use Iterator;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use SplFileObject;
use Stringable;

/**
 * StorageWriterInterface interface file.
 * 
 * This interface proposes various ways to transform storage objects and array
 * recursive structure into various flat data streams.
 * 
 * @author Anastaszor
 */
interface StorageWriterInterface extends Stringable
{
	
	/**
	 * Writes an array to the given string data.
	 * 
	 * @param StorageArrayInterface $array
	 * @return string
	 */
	public function writeArrayToString(StorageArrayInterface $array) : string;
	
	/**
	 * Writes an array to the given data in the file at given path.
	 * 
	 * @param StorageArrayInterface $array
	 * @param string $filePath
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeArrayToFile(StorageArrayInterface $array, string $filePath) : int;
	
	/**
	 * Writes an array to the given data in the given file.
	 * 
	 * @param StorageArrayInterface $array
	 * @param SplFileObject $file
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeArrayToSplFile(StorageArrayInterface $array, SplFileObject $file) : int;
	
	/**
	 * Writes an array to the given data in the given stream.
	 * 
	 * @param StorageArrayInterface $array
	 * @param StreamInterface $stream
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeArrayToStream(StorageArrayInterface $array, StreamInterface $stream) : int;
	
	/**
	 * Writes an iterator of arrays to the given string data.
	 *
	 * @param Iterator<StorageArrayInterface> $iterator
	 * @return string
	 */
	public function writeArrayIteratorToString(Iterator $iterator) : string;
	
	/**
	 * Writes an iterator of arrays to the file at the given path.
	 *
	 * @param Iterator<StorageArrayInterface> $iterator
	 * @param string $filePath
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeArrayIteratorToFile(Iterator $iterator, string $filePath) : int;
	
	/**
	 * Writes an iterator of arrays to the given file.
	 *
	 * @param Iterator<StorageArrayInterface> $iterator
	 * @param SplFileObject $file
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeArrayIteratorToSplFile(Iterator $iterator, SplFileObject $file) : int;
	
	/**
	 * Writes an iterator of arrays to the given stream.
	 *
	 * @param Iterator<StorageArrayInterface> $iterator
	 * @param StreamInterface $stream
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeArrayIteratorToStream(Iterator $iterator, StreamInterface $stream) : int;
	
	/**
	 * Writes an object to the given string data.
	 *
	 * @param StorageObjectInterface $object
	 * @return string
	 */
	public function writeObjectToString(StorageObjectInterface $object) : string;
	
	/**
	 * Writes an object to the given data in the file at given path.
	 * 
	 * @param StorageObjectInterface $object
	 * @param string $filePath
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeObjectToFile(StorageObjectInterface $object, string $filePath) : int;
	
	/**
	 * Writes an object to the given data in the given file.
	 * 
	 * @param StorageObjectInterface $object
	 * @param SplFileObject $file
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeObjectToSplFile(StorageObjectInterface $object, SplFileObject $file) : int;
	
	/**
	 * Writes an object to the given data in the given stream.
	 * 
	 * @param StorageObjectInterface $object
	 * @param StreamInterface $stream
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeObjectToStream(StorageObjectInterface $object, StreamInterface $stream) : int;
	
	/**
	 * Writes an iterator of objects to the given string data.
	 * 
	 * @param Iterator<StorageObjectInterface> $iterator
	 * @return string
	 */
	public function writeObjectIteratorToString(Iterator $iterator) : string;
	
	/**
	 * Writes an iterator of objects to the file at the given path.
	 * 
	 * @param Iterator<StorageObjectInterface> $iterator
	 * @param string $filePath
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeObjectIteratorToFile(Iterator $iterator, string $filePath) : int;
	
	/**
	 * Writes an iterator of objects to the given file.
	 * 
	 * @param Iterator<StorageObjectInterface> $iterator
	 * @param SplFileObject $file
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeObjectIteratorToSplFile(Iterator $iterator, SplFileObject $file) : int;
	
	/**
	 * Writes an iterator of objects to the given stream.
	 * 
	 * @param Iterator<StorageObjectInterface> $iterator
	 * @param StreamInterface $stream
	 * @return integer the number of bytes written
	 * @throws RuntimeException
	 */
	public function writeObjectIteratorToStream(Iterator $iterator, StreamInterface $stream) : int;
	
}
