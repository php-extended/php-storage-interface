<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-storage-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Storage;

use Countable;
use IteratorAggregate;
use Stringable;

/**
 * StorageObjectInterface interface file.
 * 
 * This interface represents a generic object that can have any attribute with
 * any value as long as the value is a primitive or one of the storage complex
 * object types (array and object).
 * 
 * @author Anastaszor
 * @extends \IteratorAggregate<string, null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface>
 */
interface StorageObjectInterface extends Countable, IteratorAggregate, Stringable
{
	
	/**
	 * Gets whether this object has an attribute value for the given name. This
	 * method returns true even if the given attribute as a null value.
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function hasAttribute(string $name) : bool;
	
	/**
	 * Gets the value this object has for the attribute with the given name.
	 * This method returns null if no attribute is available for the given name.
	 * 
	 * @param string $name
	 * @return null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface
	 */
	public function getAttribute(string $name);
	
	/**
	 * Sets the given value for the attribute at given name.
	 * 
	 * @param string $name
	 * @param null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface $value
	 * @return StorageObjectInterface
	 */
	public function setAttribute(string $name, $value) : StorageObjectInterface;
	
	/**
	 * Gets whether this storage object has exactly the same attribute names
	 * and values as the other storage object.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether this storage object contains all the attribute names and
	 * their respective values as the other storage object.
	 * 
	 * @param StorageObjectInterface $other
	 * @return boolean
	 */
	public function contains(StorageObjectInterface $other) : bool;
	
	/**
	 * Gets an array representation of this storage object. The given structure
	 * obtained by this method is recursive, as keys may be strings (for objects)
	 * or integers (for arrays) and the values are null, boolean, integer,
	 * float, string, or a recursive array of the previous.
	 * 
	 * @return array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string>>>>
	 */
	public function toArray() : array;
	
	/**
	 * Gets a new storage object that has all the fields and all the values of
	 * both storage objects. This method is not symmetrical as in case of a
	 * conflict (different values for the same attribute names), the argument's
	 * value will prevail (and this' attributes values will be lost).
	 * 
	 * @param StorageObjectInterface $other
	 * @return StorageObjectInterface
	 */
	public function unionWith(StorageObjectInterface $other) : StorageObjectInterface;
	
	/**
	 * Gets a new storage object that has only the attributes with values that
	 * are the same for both this storage object and the given storage object.
	 * 
	 * @param StorageObjectInterface $other
	 * @return StorageObjectInterface
	 */
	public function intersectWith(StorageObjectInterface $other) : StorageObjectInterface;
	
}
