<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-storage-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Storage;

use Countable;
use IteratorAggregate;
use Stringable;

/**
 * StorageArrayInterface interface file.
 * 
 * This interface represents a generic one-dimentional array that can have any
 * value for any index as long as the value is a primitive or one of the
 * storage complex object types (array and object).
 * 
 * @author Anastaszor
 * @extends \IteratorAggregate<integer, null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface>
 */
interface StorageArrayInterface extends Countable, IteratorAggregate, Stringable
{
	
	/**
	 * Gets whether this array has a value at the given index. This method
	 * returns true even if the given index contains a null value.
	 * 
	 * @param integer $idx
	 * @return boolean
	 */
	public function hasIndex(int $idx) : bool;
	
	/**
	 * Gets the value this array has for the attribute.
	 * 
	 * @param integer $idx
	 * @return null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface
	 */
	public function getIndex(int $idx);
	
	/**
	 * Sets the given value at the given index.
	 * 
	 * @param integer $idx
	 * @param null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface $value
	 * @return StorageArrayInterface
	 */
	public function setIndex(int $idx, $value) : StorageArrayInterface;
	
	/**
	 * Appends the given value at the beginning of the array.
	 * 
	 * @param null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface $value
	 * @return StorageArrayInterface
	 */
	public function prepend($value) : StorageArrayInterface;
	
	/**
	 * Appends the given value at the end of the array.
	 * 
	 * @param null|boolean|integer|float|string|StorageObjectInterface|StorageArrayInterface $value
	 * @return StorageArrayInterface
	 */
	public function append($value) : StorageArrayInterface;
	
	/**
	 * Gets whether this storage array has exactly the same indexes and values
	 * as the other storage array.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
	/**
	 * Gets whether this storage array contains all the attribute names and
	 * their respective values as the other storage array.
	 * 
	 * @param StorageArrayInterface $other
	 * @return bool
	 */
	public function contains(StorageArrayInterface $other) : bool;
	
	/**
	 * Gets an array representation of this storage array. The given structure
	 * obtained by this method is recursive, as keys may be strings (for objects)
	 * or integers (for arrays) and the values are null, boolean, integer,
	 * float, string, or a recursive array of the previous.
	 * 
	 * @return array<integer, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>>
	 */
	public function toArray() : array;
	
	/**
	 * Gets a new storage array that has all the fields and all the values of
	 * both storage arrays. This method is not symmetrical as in case of a
	 * conflict (different valus for the same indexes), the argument's value
	 * will prevail (and this' values at overridden indexes will be lost).
	 * 
	 * @param StorageArrayInterface $other
	 * @return StorageArrayInterface
	 */
	public function unionWith(StorageArrayInterface $other) : StorageArrayInterface;
	
	/**
	 * Gets a new storage array that has all the fields and all the values of
	 * both storage arrays. This method will not override any index value, and
	 * append successively this' values and the other's values. This method is
	 * not symmetrical but in both cases all the values are present (just in a
	 * different order).
	 * 
	 * @param StorageArrayInterface $other
	 * @return StorageArrayInterface
	 */
	public function mergeWith(StorageArrayInterface $other) : StorageArrayInterface;
	
	/**
	 * Gets a new storage array that has only the indexes and values that are
	 * the same for both this storage array and the given storage array.
	 * 
	 * @param StorageArrayInterface $other
	 * @return StorageArrayInterface
	 */
	public function intersectWith(StorageArrayInterface $other) : StorageArrayInterface;
	
}
