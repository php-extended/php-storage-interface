<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-storage-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Storage;

use Iterator;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use SplFileObject;
use Stringable;

/**
 * StorageReaderInterface interface file.
 * 
 * This interface proposes various ways to transform some data into a storage
 * structure.
 * 
 * @author Anastaszor
 */
interface StorageReaderInterface extends Stringable
{
	
	/**
	 * Reads an array from the given data string.
	 * 
	 * @param string $data
	 * @return StorageArrayInterface
	 * @throws RuntimeException
	 */
	public function readArrayFromString(string $data) : StorageArrayInterface;
	
	/**
	 * Reads an array from the given data in the file at given path.
	 * 
	 * @param string $filePath
	 * @return StorageArrayInterface
	 * @throws RuntimeException
	 */
	public function readArrayFromFile(string $filePath) : StorageArrayInterface;
	
	/**
	 * Reads an array from the given data in the given file.
	 * 
	 * @param SplFileObject $file
	 * @return StorageArrayInterface
	 * @throws RuntimeException
	 */
	public function readArrayFromSplFile(SplFileObject $file) : StorageArrayInterface;
	
	/**
	 * Reads an array from the given data in the given stream.
	 * 
	 * @param StreamInterface $stream
	 * @return StreamInterface
	 * @throws RuntimeException
	 */
	public function readArrayFromStream(StreamInterface $stream) : StreamInterface;
	
	/**
	 * Reads an iterator of arrays from the given data string.
	 *
	 * @param string $data
	 * @return Iterator<StorageArrayInterface>
	 * @throws RuntimeException
	 */
	public function readArrayIteratorFromString(string $data) : Iterator;
	
	/**
	 * Reads an iterator of arrays from the data at the given path.
	 *
	 * @param string $filePath
	 * @return Iterator<StorageArrayInterface>
	 * @throws RuntimeException
	 */
	public function readArrayIteratorFromFile(string $filePath) : Iterator;
	
	/**
	 * Reads an iterator of arrays from the data in the given file.
	 *
	 * @param SplFileObject $file
	 * @return Iterator<StorageArrayInterface>
	 * @throws RuntimeException
	 */
	public function readArrayIteratorFromSplFile(SplFileObject $file) : Iterator;
	
	/**
	 * Reads an iterator of arrays from the data in the given stream.
	 *
	 * @param StreamInterface $stream
	 * @return Iterator<StorageArrayInterface>
	 * @throws RuntimeException
	 */
	public function readArrayIteratorFromStream(StreamInterface $stream) : Iterator;
	
	/**
	 * Reads an object from the given data string.
	 * 
	 * @param string $data
	 * @return StorageObjectInterface
	 * @throws RuntimeException
	 */
	public function readObjectFromString(string $data) : StorageObjectInterface;
	
	/**
	 * Reads an object from the given data in the file at given path.
	 * 
	 * @param string $filePath
	 * @return StorageObjectInterface
	 * @throws RuntimeException
	 */
	public function readObjectFromFile(string $filePath) : StorageObjectInterface;
	
	/**
	 * Reads an object from the given data in the given file.
	 * 
	 * @param SplFileObject $file
	 * @return StorageObjectInterface
	 * @throws RuntimeException
	 */
	public function readObjectFromSplFile(SplFileObject $file) : StorageObjectInterface;
	
	/**
	 * Reads an object from the given data in the given stream.
	 * 
	 * @param StreamInterface $stream
	 * @return StorageObjectInterface
	 * @throws RuntimeException
	 */
	public function readObjectFromStream(StreamInterface $stream) : StorageObjectInterface;
	
	/**
	 * Reads an iterator of objects from the given data string.
	 * 
	 * @param string $data
	 * @return Iterator<StorageObjectInterface>
	 * @throws RuntimeException
	 */
	public function readObjectIteratorFromString(string $data) : Iterator;
	
	/**
	 * Reads an iterator of objects from the data at the given path.
	 * 
	 * @param string $filePath
	 * @return Iterator<StorageObjectInterface>
	 * @throws RuntimeException
	 */
	public function readObjectIteratorFromFile(string $filePath) : Iterator;
	
	/**
	 * Reads an iterator of objects from the data in the given file.
	 * 
	 * @param SplFileObject $file
	 * @return Iterator<StorageObjectInterface>
	 * @throws RuntimeException
	 */
	public function readObjectIteratorFromSplFile(SplFileObject $file) : Iterator;
	
	/**
	 * Reads an iterator of objects from the data in the given stream.
	 * 
	 * @param StreamInterface $stream
	 * @return Iterator<StorageObjectInterface>
	 * @throws RuntimeException
	 */
	public function readObjectIteratorFromStream(StreamInterface $stream) : Iterator;
	
}
